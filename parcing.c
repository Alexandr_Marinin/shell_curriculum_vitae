/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parcing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 18:13:41 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/26 16:55:59 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_parcer_2(char **line, char **env, t_parcer *st)
{
	st->buf[st->j] = '\0';
	if ((*line)[st->i] == '\\' && (*line)[st->i + 1])
	{
		st->i += 1;
	}
	else if ((*line)[st->i] == '$' && st->symbol != '\'')
	{
		if (!(ft_parcing_path(line, st, env)))
		{
			free(st->buf);
			return (0);
		}
		st->len_buf = ft_strlen(st->buf);
	}
	else if ((*line)[st->i] == '~' && st->symbol == '!')
	{
		if (!(ft_insert_home(&st->buf, &st->j, env)))
		{
			free(st->buf);
			return (0);
		}
		st->i++;
	}
	return (1);
}

int	ft_parcer_3(char **line, t_parcer *st)
{
	if (st->len_buf <= st->j)
	{
		st->len_buf += 32;
		st->buf_buf = st->buf;
		if (!(st->buf = (char*)malloc(sizeof(char) * (st->len_buf + 1))))
		{
			free(st->buf_buf);
			return (0);
		}
		ft_strcpy(st->buf, st->buf_buf);
		free(st->buf_buf);
	}
	st->buf[st->j] = (*line)[st->i];
	st->i++;
	st->j++;
	return (1);
}

int	ft_parcer_return(t_parcer *st, char **line)
{
	st->buf[st->j] = '\0';
	free(*line);
	*line = st->buf;
	return (1);
}

int	ft_parcer(char **line, char **env)
{
	t_parcer	st;

	if (!(*line))
		return (0);
	if (!(st.symbol = ft_count_symbol(*line)))
		return (2);
	st.len_buf = ft_strlen(*line);
	st.len_line = st.len_buf;
	if (!(st.buf = (char*)malloc(sizeof(char) * (st.len_buf + 1))))
		return (0);
	st.i = 0;
	st.j = 0;
	while (st.i < st.len_line && (*line)[st.i])
	{
		if (!(ft_parcer_2(line, env, &st)))
			return (0);
		if ((*line)[st.i] == st.symbol)
		{
			st.i++;
			continue;
		}
		if (!(ft_parcer_3(line, &st)))
			return (0);
	}
	return (ft_parcer_return(&st, line));
}

int	ft_parc_struct(t_command **command, char **env)
{
	t_command	*list;
	int			i;
	int			x;

	list = *command;
	i = 1;
	x = 0;
	while (list)
	{
		while (list->command && list->command[0] && list->command[i])
		{
			x = ft_parcer(&(list->command[i]), env);
			if (!x)
				exit(0);
			else if (x == 2)
				return (2);
			i++;
		}
		i = 1;
		list = list->next;
	}
	return (1);
}
