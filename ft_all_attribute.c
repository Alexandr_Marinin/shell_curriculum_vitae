/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_all_attribute.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 18:02:57 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/26 17:22:42 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_auto	*ft_all_attribute_5(char **dup, t_spisok **spisok)
{
	free(*dup);
	ft_free_t_spisok(spisok);
	return (NULL);
}

t_auto	*ft_all_attribute_4(t_bor **bor, char **dup, t_auto **spisok_auto)
{
	free(*bor);
	if (*dup)
		free(*dup);
	return (*spisok_auto);
}

int		ft_all_attribute_3(char **slash, char *left_word, char **dup,
		t_spisok **spisok)
{
	if ((*slash) - left_word == 0)
	{
		if (!((*dup) = ft_strndup("/", 1)))
			return (0);
	}
	else
	{
		if (!((*dup) = ft_strndup(left_word, (*slash) - left_word)))
			return (0);
	}
	if (ft_allocate_atribute(*dup, spisok) == -1)
	{
		free(*dup);
		return (0);
	}
	return (1);
}

int		ft_all_attribute_2(t_spisok **spisok, char *left_word, t_bor **bor_buf,
		t_bor **bor)
{
	if (ft_allocate_atribute("./", spisok) == -1)
		return (0);
	if (!((*bor) = ft_in_bor(*spisok)))
	{
		ft_free_t_spisok(spisok);
		return (0);
	}
	ft_free_t_spisok(spisok);
	*bor_buf = ft_search_entry(left_word, *bor);
	return (1);
}

t_auto	*ft_all_attribute(char *left_word, t_bor **bor)
{
	t_spisok	*spisok;
	t_auto		*spisok_auto;
	t_bor		*bor_buf;
	char		*a[2];

	spisok = NULL;
	spisok_auto = NULL;
	(a[1]) = NULL;
	if (!((a[0]) = ft_strrchr(left_word, '/')))
	{
		if (!(ft_all_attribute_2(&spisok, left_word, &bor_buf, bor)))
			return (NULL);
		spisok_auto = ft_print_bor(bor_buf, bor_buf, &spisok_auto);
		ft_free_bor(bor);
	}
	else
	{
		if (!(ft_all_attribute_3(&(a[0]), left_word, &(a[1]), &spisok)))
			return (NULL);
		if (!(*bor = ft_in_bor(spisok)))
			return (ft_all_attribute_5(&(a[1]), &spisok));
		ft_free_t_spisok(&spisok);
		ft_all_attribute_6(a[0], &bor_buf, bor, &spisok_auto);
	}
	return ((ft_all_attribute_4(bor, &(a[1]), &spisok_auto)));
}
