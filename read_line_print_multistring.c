#include "minishell.h"

void	ft_back_counter_colums(t_read_line *read_line, t_term **term_capability, t_print *print_struct)
{
	if (print_struct->start_pos_curs == print_struct->finish_pos_curs
		&& print_struct->start_line_curs == print_struct->finish_line_curs)
		return ;
	while(print_struct->start_line_curs != print_struct->finish_line_curs || print_struct->start_pos_curs != print_struct->finish_pos_curs)
	{
		if (print_struct->start_line_curs < print_struct->finish_line_curs)
		{
			write(1, (*term_capability)->move_up_row, ft_strlen((*term_capability)->move_up_row));
			print_struct->finish_line_curs--;
		}
		if (print_struct->start_pos_curs > print_struct->finish_pos_curs)
		{
			write(1, (*term_capability)->move_right, ft_strlen((*term_capability)->move_right));
			print_struct->finish_pos_curs++;
		}
		else if (print_struct->start_pos_curs < print_struct->finish_pos_curs)
		{
			write(1, (*term_capability)->move_left, ft_strlen((*term_capability)->move_left));
			print_struct->finish_pos_curs--;
		}
	}
}

size_t	ft_how_many_character_to_end_line(char *line, size_t *finish_pos_curs, size_t pointer_before, size_t ws_col_size, int *move_cursor, size_t *start_pos_curs)
{
	size_t	pos_array;

	pos_array = 0;
	while (pos_array < pointer_before && (*finish_pos_curs) < ws_col_size)
	{
		(*finish_pos_curs)++;
		pos_array++;
		if ((move_cursor > 0))
			(*move_cursor)--;
		if ((*move_cursor) == 0)
		{
			(*start_pos_curs) = (*finish_pos_curs);
			(*move_cursor)--;
		}
	}
	return (pos_array);
}

int ft_print_multistring(char *line, size_t size_print, size_t ws_col_size, t_term **term_capability, t_print	*print_struct)
{
	size_t	print_size;
	size_t	start_print;

	print_size = 0;
	start_print = 0;
	while (size_print > 0)
	{
		print_size = ft_how_many_character_to_end_line(&(line[start_print]), &(print_struct->finish_pos_curs), size_print, ws_col_size, &(print_struct->move_cursor), &(print_struct->start_pos_curs));
		write(1, &(line[start_print]), print_size);
		size_print -= print_size;
		start_print += print_size;
		if (size_print > 0)
		{
			write(1, "\n\r", 2);
			print_struct->finish_pos_curs = 0;
			print_struct->finish_line_curs += 1;
			if (print_struct->move_cursor > 0)
				print_struct->start_line_curs += 1;
		}
	}
	return (1);
}

void	ft_print_text_multistring(char *line, size_t size_print, t_term **term_capability, t_read_line *read_line, int move_cursor)
{
	t_print		print_struct;
	print_struct.start_pos_curs = read_line->colums_cursor;
	print_struct.start_line_curs = read_line->line_cursor;
	print_struct.finish_pos_curs = read_line->colums_cursor;
	print_struct.finish_line_curs = read_line->line_cursor;
	print_struct.move_cursor = move_cursor;

	if (!(size_print))
		return ;
	ft_print_multistring(line, size_print, read_line->ws_colums - 1, term_capability, &print_struct);
	read_line->max_column_cursor = print_struct.finish_pos_curs;
	read_line->max_line_cursor = print_struct.finish_line_curs;
	read_line->colums_cursor = print_struct.start_pos_curs;
	read_line->line_cursor = print_struct.start_line_curs;
	ft_back_counter_colums(read_line, term_capability, &print_struct);
}
