/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 21:17:42 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/20 17:24:42 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_env(char **env)
{
	int i;

	i = 0;
	while (env[i])
	{
		ft_putendl(env[i]);
		i++;
	}
}

char	*ft_putendl_and_return_null(char *text)
{
	ft_putendl(text);
	return (NULL);
}

int		ft_unsetenv_2(char **argv, char ***del_arg, char ***env, int *i)
{
	if (!(argv[1]) || argv[2])
		return ((int)ft_putendl_and_return_null("usage: unsetenv variable"));
	if (!(*del_arg = ft_getpath(argv[1], *env)))
		return ((int)ft_putendl_and_return_null("variable not found"));
	while ((*env)[(*i)])
		(*i)++;
	return (1);
}

int		ft_unsetenv(char **argv, char ***env)
{
	int		i;
	int		j;
	char	**new_env;
	char	**del_arg;

	j = 0;
	if ((i = 0) || !(ft_unsetenv_2(argv, &del_arg, env, &i)))
		return (0);
	if (!(new_env = (char**)malloc(sizeof(char*) * (i))))
		return ((int)ft_putendl_and_return_null("not work malloc in unsetenv"));
	i = 0;
	while ((*env)[i])
	{
		if ((*del_arg) == (*env)[i])
		{
			free(*del_arg);
			i++;
			continue;
		}
		new_env[j++] = (*env)[i++];
	}
	new_env[j] = NULL;
	free(*env);
	(*env) = new_env;
	return (1);
}

char	**ft_copy_env(char **env)
{
	int		i;
	char	**new_env;

	i = 0;
	while (env[i])
	{
		i++;
	}
	if (!(new_env = (char**)malloc(sizeof(char*) * (i + 1))))
		return (NULL);
	i = 0;
	while (env[i])
	{
		if (!(new_env[i] = ft_strdup(env[i])))
		{
			ft_free_array(&new_env);
			return (NULL);
		}
		i++;
	}
	new_env[i] = NULL;
	return (new_env);
}
