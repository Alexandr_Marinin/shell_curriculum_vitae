/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   autocompletion_launch_print.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 21:15:06 by fwyman            #+#    #+#             */
/*   Updated: 2019/07/23 21:00:41 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

size_t	ft_len_symbol_from_left_world_cursor(char *buf_line)
{
	size_t i;
	size_t counter;

	i = 0;
	counter = 0;
	while (buf_line[i])
	{
		if (buf_line[i] == '/')
		{
			counter = -1;
		}
		i++;
		counter++;
	}
	return (counter);
}

char	*ft_world_left_of_cursor(char *line, size_t pointer_cursor,
		size_t *integer_left_symbol_cursor)
{
	char	*out;
	size_t	i;

	i = pointer_cursor;
	while (pointer_cursor > 0
		&& line[pointer_cursor - 1] > 32 && line[pointer_cursor - 1] != ';')
	{
		if (pointer_cursor)
			pointer_cursor--;
		else
			break ;
	}
	if (!(out = ft_strndup(&line[pointer_cursor], (i - pointer_cursor))))
	{
		ft_putendl("malloc don't work");
		exit(0);
	}
	out[(i - pointer_cursor)] = '\0';
	(*integer_left_symbol_cursor) = i - pointer_cursor;
	return (out);
}

t_auto	*ft_filling_bor_command_or_attribute(char **buf_line,
		size_t *i_left_symbol_cursor, t_bor **start_prog, int a)
{
	t_bor	*buf_bor;
	t_auto	*arrg;

	buf_bor = NULL;
	arrg = NULL;
	if (a == 1)
	{
		if (!(buf_bor = ft_search_entry(*buf_line, *start_prog)))
		{
			free(*buf_line);
			return (NULL);
		}
		free(*buf_line);
		arrg = ft_print_bor(buf_bor, buf_bor, &arrg);
	}
	else if (a == 2)
	{
		(*i_left_symbol_cursor) =
			ft_len_symbol_from_left_world_cursor(*buf_line);
		arrg = ft_all_attribute(*buf_line, &buf_bor);
		free(*buf_line);
	}
	return (arrg);
}

int		ft_launch_print_autocompletion(t_read_line *read_line, t_term **term_capability, char *buf,
		t_bor **start_prog, int a)
{
	size_t	i_left_symbol_cursor;
	char	*buf_line;
	t_auto	*arrg;

	arrg = NULL;
	if (!(buf_line = ft_world_left_of_cursor(read_line->line,
					read_line->pointer_cursor, &i_left_symbol_cursor)))
		return (1);
	if (!(arrg = ft_filling_bor_command_or_attribute(&buf_line,
					&i_left_symbol_cursor, start_prog, a))
	|| (ft_print_autocompletion(read_line, term_capability, buf, &arrg, i_left_symbol_cursor)))
		return (1);
	else
		return (0);
}
