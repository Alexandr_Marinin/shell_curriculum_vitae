/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_autocompletion.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 19:16:25 by fwyman            #+#    #+#             */
/*   Updated: 2019/07/16 20:16:06 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_paste_to_array(t_read_line *read_line, char *paste,
		size_t length_paste)
{
	size_t size;

	size = (read_line->pointer_last_symbol) - (read_line->pointer_cursor);
	(read_line->pointer_last_symbol) += length_paste;
	ft_check_and_expansion_array(&(read_line->line),
			read_line->pointer_last_symbol, &(read_line->length_line));
	ft_memmove(&((read_line->line)[(read_line->pointer_cursor) + length_paste]),
			&((read_line->line)[(read_line->pointer_cursor)]), size);
	ft_memmove(&((read_line->line)[(read_line->pointer_cursor)]), paste,
			length_paste);
}

size_t	ft_print_word(t_read_line *read_line, t_term **term_capability, char *word,
		size_t integer_left_symbol_cursor)
{
	char	*str;
	size_t	i;
	size_t	i1;

	i = 0;
	if (!(str = ft_strrchr(word, '/') + integer_left_symbol_cursor + 1))
		return (0);

	ft_paste_to_array(read_line, str, i = ft_strlen(str));
	ft_print_text_multistring(&(read_line->line[read_line->pointer_cursor]), ((read_line->pointer_last_symbol) - (read_line->pointer_cursor))
	, term_capability, read_line, i);
	(read_line->pointer_cursor) += i;
	return (i);
}

void	ft_del_to_array(char **line, size_t pointer_last_symbol,
		size_t pointer_cursor, size_t count_del_symbol)//, t_read_line *read_line)
{
	ft_memmove(&((*line)[pointer_cursor - count_del_symbol]),
			&((*line)[pointer_cursor]), pointer_last_symbol - pointer_cursor);
}

void	ft_del_last_world(char **line, size_t *pointer_last_symbol,
		size_t *pointer_cursor, size_t count_auto_symbol, t_read_line *read_line, t_term **term_capability)
{
	size_t	i;

	i = count_auto_symbol;
	while (i--)
	{
		ft_remove_to_array(read_line);
	}
}

int		ft_print_autocompletion(t_read_line *read_line, t_term **term_capability, char *buf, t_auto **arrg,
		size_t i_left_symbol_cursor)
{
	size_t	count_auto_symbol;
	t_auto	*buffer;

	count_auto_symbol = 0;
	if (!(arrg))
		write(1, "\nno arrg\n", 9);
	buffer = *arrg;
	while (buffer)
	{
		count_auto_symbol = ft_print_word(read_line, term_capability,(buffer)->full_way,
				i_left_symbol_cursor);
		ft_bzero(&(*buf), 10);
		read(0, &(*buf), 4);
		if (buf[0] != 9)
		{
			ft_free_t_auto(arrg);
			return (0);
		}
		ft_del_last_world(&(read_line->line), &(read_line->pointer_last_symbol),
				&(read_line->pointer_cursor), count_auto_symbol, read_line, term_capability);
		(buffer) = (buffer)->next;
	}
	ft_free_t_auto(arrg);
	return (1);
}
