/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_line_delete.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 18:49:28 by fwyman            #+#    #+#             */
/*   Updated: 2019/08/27 19:50:03 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_write_spase(int i)
{
	while(i-- > 0)
	{
		write(1, " ", 1);
	}
}

void	ft_remove_tail_line(t_read_line *read_line, t_term **term_capability)
{
	t_print print;

	print.start_pos_curs = read_line->colums_cursor;
	print.start_line_curs = read_line->line_cursor;
	print.finish_pos_curs = read_line->max_column_cursor;
	print.finish_line_curs = read_line->max_line_cursor;
	while(print.start_pos_curs != print.finish_pos_curs || print.start_line_curs != print.finish_line_curs)
	{
		if (print.start_line_curs == print.finish_line_curs)
		{
			ft_write_spase(print.finish_pos_curs - print.start_pos_curs);
			print.start_pos_curs = read_line->colums_cursor;
			print.start_line_curs = read_line->line_cursor;
			break;
		}
		else
		{
			ft_write_spase((read_line->ws_colums - 1) - print.start_pos_curs);
			write(1, "\n\r", 2);
			print.start_pos_curs = 0;
			(print.start_line_curs)++;
		}
	}
	ft_back_counter_colums(read_line, term_capability, &print);
}

void	ft_remove_to_array(t_read_line *read_line)//, t_term **term_capability)
{
	if(!(read_line->colums_cursor))
		ft_move_left_cursor(read_line, &(read_line->term_capability));
	ft_move_left_cursor(read_line, &(read_line->term_capability));
	ft_del_to_array(&(read_line->line), read_line->pointer_last_symbol, read_line->pointer_cursor + 1, 1);
	ft_remove_tail_line(read_line, &(read_line->term_capability));
	(read_line->pointer_last_symbol)--;
	ft_print_text_multistring(&(read_line->line[read_line->pointer_cursor]), ((read_line->pointer_last_symbol) - (read_line->pointer_cursor))
	, &(read_line->term_capability), read_line, 0);
}
