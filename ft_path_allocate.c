/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_path_allocate.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 17:59:58 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/15 18:38:19 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_error_allocate(DIR *directory, t_spisok **zveno)
{
	closedir(directory);
	if (*zveno)
		ft_free_t_spisok(zveno);
	return (-1);
}

int	ft_path_allocate_2(t_spisok **zveno, t_spisok **node, struct dirent	*entry
	, DIR *directory)
{
	if (!access((*zveno)->content, 1))
	{
		if (!node)
		{
			*node = (*zveno);
			if (!((*zveno)->name = ft_strdup(entry->d_name)))
				return (ft_error_allocate(directory, zveno));
			(*zveno)->next = NULL;
		}
		else
		{
			(*zveno)->next = *node;
			if (!((*zveno)->name = ft_strdup(entry->d_name)))
				return (ft_error_allocate(directory, zveno));
			*node = (*zveno);
		}
		if (!((*zveno) = ft_allocate_t_spisok()))
			return (ft_error_allocate(directory, zveno));
	}
	else
	{
		free((*zveno)->content);
		(*zveno)->content = NULL;
	}
	return (0);
}

int	ft_path_allocate(char *path_way, t_spisok **node)
{
	struct dirent	*entry;
	DIR				*directory;
	t_spisok		*zveno;
	int				len_path;

	if (!(directory = opendir(path_way)))
		return (-2);
	if (!(zveno = ft_allocate_t_spisok()))
		return (ft_error_allocate(directory, &zveno));
	len_path = ft_strlen(path_way);
	while ((entry = readdir(directory)))
	{
		if (*(entry->d_name) == '.')
			continue;
		if (!(zveno->content = ft_full_way_to_file(path_way, entry->d_name
			, len_path)))
			return (ft_error_allocate(directory, &zveno));
		if ((ft_path_allocate_2(&zveno, node, entry, directory)) == -1)
			return (-1);
	}
	free(zveno);
	closedir(directory);
	return (0);
}
