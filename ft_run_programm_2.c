/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_run_programm_2.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 19:37:58 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/26 17:04:10 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_check_programm(char *path)
{
	struct stat buf;

	if (access(path, 0))
	{
		ft_putendl("no such file or directory");
		return (0);
	}
	if (access(path, 1))
	{
		ft_putstr("not executable file: ");
		ft_putendl(path);
		return (0);
	}
	if (stat(path, &buf) == -1)
		return (0);
	if (!S_ISREG(buf.st_mode))
	{
		ft_putstr("permission denied: ");
		ft_putendl(path);
		return (0);
	}
	return (1);
}
