/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 19:16:15 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/15 19:16:38 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*ft_allocate_path(char **env_path, char *path, char *arg)
{
	int len;
	int i;
	int j;

	i = 0;
	j = 0;
	len = ft_strlen(path) + ft_strlen(arg);
	if (!(*env_path = (char*)malloc(sizeof(char) * (len + 2))))
		return (NULL);
	while (path[i])
	{
		(*env_path)[i] = path[i];
		i++;
	}
	(*env_path)[i] = '=';
	i++;
	while (arg[j])
	{
		(*env_path)[i] = arg[j];
		i++;
		j++;
	}
	(*env_path)[i] = '\0';
	return (*env_path);
}

char	**ft_extend_env(char **env, char *path, char *arg)
{
	int		len_env;
	char	**new_env;

	len_env = 0;
	while (env[len_env])
		len_env++;
	if (!(new_env = (char**)malloc(sizeof(char*) * (len_env + 2))))
		return (NULL);
	len_env = 0;
	while (env[len_env])
	{
		new_env[len_env] = env[len_env];
		len_env++;
	}
	if (!(new_env[len_env] = ft_allocate_path(&new_env[len_env], path, arg)))
	{
		ft_free_array(&new_env);
		return (NULL);
	}
	len_env++;
	new_env[len_env] = NULL;
	return (new_env);
}

char	**ft_getpath(char *path, char **env)
{
	size_t	i;
	size_t	len_path;

	i = 0;
	len_path = ft_strlen(path);
	if (!(env) || !(path))
		return (NULL);
	while (env[i])
	{
		if (!(ft_strncmp(env[i], path, len_path)) && env[i][len_path] == '=')
			return (&env[i]);
		i++;
	}
	return (NULL);
}

int		ft_setenv_2(char ***env_buf, char ***env, char **argv)
{
	if (!(*env_buf = ft_extend_env(*env, argv[1], argv[2])))
	{
		ft_putendl("malloc error in setenv");
		return (0);
	}
	else
	{
		free(*env);
		*env = *env_buf;
	}
	return (1);
}

char	**ft_setenv(char ***env, char **argv)
{
	char	**change_path;
	char	**env_buf;
	char	*path_buf;

	if (!(*argv && argv[1] && argv[2]))
	{
		ft_putendl("usage: setenv path value");
		return (*env);
	}
	if ((change_path = ft_getpath(argv[1], *env)))
	{
		path_buf = *change_path;
		if (!(ft_allocate_path(change_path, argv[1], argv[2])))
		{
			*change_path = path_buf;
			ft_putendl("malloc error in setenv");
			return (NULL);
		}
		else
			free(path_buf);
	}
	else if (!(ft_setenv_2(&env_buf, env, argv)))
		return (NULL);
	return (*env);
}
