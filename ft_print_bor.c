/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_bor.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 18:21:06 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/15 18:38:13 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_print_bor_4(t_auto **spisok, t_bor **zveno)
{
	if (!((*spisok) = ft_allocate_t_auto()))
		return (0);
	if (!((*spisok)->full_way = ft_strdup((*zveno)->full_way)))
	{
		free(spisok);
		return (0);
	}
	(*spisok)->next = NULL;
	return (1);
}

int		ft_print_bor_3(t_auto **spisok, t_bor **zveno, t_auto **node)
{
	if (!(*spisok))
	{
		if (!(ft_print_bor_4(spisok, zveno)))
			return (0);
	}
	else
	{
		if (!((*node) = ft_allocate_t_auto()))
		{
			ft_free_t_auto(spisok);
			return (0);
		}
		if (!((*node)->full_way = ft_strdup((*zveno)->full_way)))
		{
			free(*node);
			ft_free_t_auto(spisok);
			return (0);
		}
		(*node)->next = *spisok;
		*spisok = (*node);
	}
	return (1);
}

t_auto	*ft_print_bor(t_bor *start1, t_bor *start, t_auto **spisok)
{
	t_bor	*zveno;
	t_auto	*node;

	node = NULL;
	if (!(zveno = start))
		return (NULL);
	while (zveno)
	{
		if (zveno->right && zveno != start1)
			ft_print_bor(start1, zveno->right, spisok);
		if (zveno->end)
		{
			if (!(ft_print_bor_3(spisok, &zveno, &node)))
				return (NULL);
		}
		zveno = zveno->down;
	}
	return (*spisok);
}

t_auto	*ft_print_bor2(t_bor *start1, t_bor *start, t_auto **spisok)
{
	t_bor	*zveno;
	t_auto	*node;

	node = NULL;
	if (!(zveno = start))
		return (NULL);
	while (zveno)
	{
		if (zveno->right)
			ft_print_bor(start1, zveno->right, spisok);
		if (zveno->end)
		{
			if (!(ft_print_bor_3(spisok, &zveno, &node)))
				return (NULL);
		}
		zveno = zveno->down;
	}
	return (*spisok);
}
