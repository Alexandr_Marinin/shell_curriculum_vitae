/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_run_programm.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 18:10:40 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/26 17:07:48 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_run_program(char *path, char **argv, char **env)
{
	pid_t	pid;
	int		status;

	signal(SIGINT, ft_signal);
	if ((pid = fork()) == 0)
	{
		if (execve(path, argv, env) == -1)
		{
			ft_putendl("program launch error");
			exit(EXIT_FAILURE);
			return (0);
		}
	}
	else if (pid < 0)
	{
		ft_putendl("failed fork");
		return (0);
	}
	waitpid(pid, &status, WUNTRACED);
	while (!WIFEXITED(status) && !WIFSIGNALED(status))
		waitpid(pid, &status, WUNTRACED);
	return (1);
}

int	ft_exit(void)
{
	exit(0);
}

int	ft_echo(char *path, char **argv)
{
	int i;

	i = 1;
	if (!path || !argv || !argv[0])
		return (0);
	while (argv[i])
	{
		ft_putstr(argv[i]);
		i++;
		if (argv[i])
			ft_putchar(' ');
	}
	ft_putchar('\n');
	return (1);
}

int	ft_shell_prog(char *path, char **argv, char ***env)
{
	if (!ft_strcmp(path, "cd"))
		ft_cd(argv, *env);
	else if (!ft_strcmp(path, "echo"))
		ft_echo(path, argv);
	else if (!ft_strcmp(path, "setenv"))
		ft_setenv(env, argv);
	else if (!ft_strcmp(path, "unsetenv"))
		ft_unsetenv(argv, env);
	else if (!ft_strcmp(path, "env"))
		ft_env(*env);
	else if (!ft_strcmp(path, "exit"))
		ft_exit();
	else
		return (1);
	return (0);
}

int	ft_pre_run_program(char *path, char **argv, char ***env, t_bor **bor)
{
	t_bor *bor_prog;

	if (!path || !argv)
		return (0);
	if (!ft_shell_prog(path, argv, env))
		return (1);
	if (!(bor_prog = ft_search_entry(path, *bor)))
	{
		if (!ft_check_programm(path))
			return (0);
		else
			ft_run_program(path, argv, *env);
	}
	else
	{
		if (!ft_check_programm(bor_prog->full_way))
			return (0);
		else
			ft_run_program(bor_prog->full_way, argv, *env);
	}
	return (1);
}
