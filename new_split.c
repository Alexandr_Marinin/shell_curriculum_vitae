/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_split.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 19:31:50 by fwyman            #+#    #+#             */
/*   Updated: 2019/07/15 19:31:52 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

size_t	ft_search_world(char *str)
{
	size_t i;

	i = 0;
	while (str[i])
	{
		if (str[i] < 33)
		{
			return (i);
		}
		i++;
	}
	return (i);
}

size_t	ft_search_symbol_split(char *str)
{
	size_t	i;
	char	c;

	i = 1;
	c = str[0];
	while (str[i])
	{
		if (str[i] == c)
		{
			i++;
			if (str[i] > 32)
				i += ft_search_world(&str[i]);
			return (i);
		}
		i++;
	}
	return (i);
}

size_t	ft_counter_split(char *str)
{
	size_t	i;
	size_t	i1;

	i = 0;
	i1 = 0;
	while (str[i])
	{
		if (str[i] != ' ')
		{
			if (str[i] == '"' || str[i] == '\'')
				i += ft_search_symbol_split(&(str[i]));
			else
				i += ft_search_world(&(str[i]));
			i1++;
		}
		else
			i++;
	}
	return (i1);
}

char	**ft_new_split(char *str)
{
	char	**buf;
	size_t	i;
	size_t	i1;
	size_t	i_buf;

	i = 0;
	i1 = 0;
	if (!str || !(buf = (char**)malloc(sizeof(char*) *
					(ft_counter_split(str) + 1))))
		return (NULL);
	while (str[i])
		if (str[i] != ' ')
		{
			if (str[i] == '"' || str[i] == '\'')
				buf[i1++] = ft_strndup(&(str[i]),
						i_buf = ft_search_symbol_split(&(str[i])));
			else
				buf[i1++] = ft_strndup(&(str[i]),
						i_buf = ft_search_world(&(str[i])));
			i += i_buf;
		}
		else
			i++;
	buf[i1] = NULL;
	return (buf);
}
