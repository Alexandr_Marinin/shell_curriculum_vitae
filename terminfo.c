#include "minishell.h"

void	ft_launch_terminfo(char **term_buffer)
{
	char	*termtype;
	int		success;

	if (!(termtype = getenv("TERM")))
		ft_write_error("Error ENV.\n");
	if (!((*term_buffer) = (char*)malloc(2048)))
		ft_write_error("malloc don't work!");
	success = tgetent(*term_buffer, termtype);
	if (success < 0)
    	ft_write_error("Could not access the termcap data base.");
	else if (success == 0)
    	ft_write_error("Terminal type is not defined.");
}

void	ft_filling_terminfo(t_term **term_capability, size_t size)
{
	char	*buffer;

	if(!((*term_capability) = (t_term*)malloc(sizeof(t_term)))
	|| !(buffer = (char*)malloc(size)))
		ft_write_error("malloc don't work!");
	if (!((*term_capability)->left = tgetstr("kl", &buffer))
	|| !((*term_capability)->right = tgetstr("kr", &buffer))
	|| !((*term_capability)->move_left = tgetstr("le", &buffer))
	|| !((*term_capability)->move_right = tgetstr("nd", &buffer))
	|| !((*term_capability)->move_start_down_row = tgetstr("do", &buffer))
	|| !((*term_capability)->move_up_row = tgetstr("up", &buffer)))
		ft_write_error("Capability is not present in the terminal description!");
}


void	ft_terminfo(t_term **term_capability)
{
	char	*term_buffer;
	char	*buf;

	ft_launch_terminfo(&term_buffer);

	if ((buf = tgetstr("ks", NULL)))
		ft_putstr(buf);
	else
		ft_write_error("termcap not keypad mode!");

	ft_filling_terminfo(term_capability, ft_strlen(term_buffer));
}
