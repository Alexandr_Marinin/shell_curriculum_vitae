/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mics.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 19:31:36 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/23 19:14:05 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char		*ft_full_way_to_file(char *path_way, char *name, int len_path)
{
	char *array;
	char *start;

	if (!(array = (char*)malloc(sizeof(char) *
	(len_path + ft_strlen(name) + 2))))
		return (NULL);
	start = array;
	while (*path_way)
		*array++ = *path_way++;
	*array++ = '/';
	while (*name)
		*array++ = *name++;
	*array = '\0';
	return (start);
}

char		*ft_free_array(char ***array)
{
	char **arr;

	arr = *array;
	while (*arr)
	{
		free(*arr);
		arr++;
	}
	free(*array);
	return (NULL);
}

t_spisok	*ft_split_path(char *path)
{
	char		**line;
	t_spisok	*a;
	char		*buf;
	int			i;

	line = NULL;
	a = NULL;
	i = 0;
	if (!(line = ft_strsplit(path, ':')))
		return (NULL);
	if (!(buf = ft_strdup(ft_strchr(line[0], '/'))))
		return ((t_spisok*)ft_free_array(&line));
	free(line[0]);
	line[0] = buf;
	while (line[i])
	{
		if ((ft_path_allocate(line[i], &a)) == -1)
			return ((t_spisok*)ft_free_array(&line));
		i++;
	}
	ft_free_array(&line);
	return (a);
}

t_auto		*ft_allocate_t_auto(void)
{
	t_auto *list;

	if (!(list = (t_auto*)malloc(sizeof(t_auto))))
		return (NULL);
	list->full_way = NULL;
	list->next = NULL;
	return (list);
}

void		ft_free_t_auto(t_auto **list)
{
	t_auto *next_list;

	while (*list)
	{
		next_list = (*list)->next;
		if ((*list)->full_way)
			free((*list)->full_way);
		free(*list);
		(*list) = next_list;
	}
}
