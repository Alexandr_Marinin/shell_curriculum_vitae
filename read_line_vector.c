/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_line_vector.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 19:08:56 by fwyman            #+#    #+#             */
/*   Updated: 2019/08/27 19:17:48 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_check_and_expansion_array(char **line, size_t pointer_last_symbol,
		size_t *length_line)
{
	char	*array_buffer;

	if (pointer_last_symbol >= (*length_line))
	{
		(*length_line) *= 2;
		if (!(array_buffer = ft_strnew(*length_line))
		|| !ft_strcpy(array_buffer, *line))
		{
			write(2, "malloc don't work2\n", 19);
			return (0);
		}
		free(*line);
		(*line) = array_buffer;
	}
	return (1);
}

void	ft_print(t_term **term_capability, t_read_line *read_line)
{
	size_t i;

	ft_print_text_multistring(&(read_line->line[read_line->pointer_cursor]), ((read_line->pointer_last_symbol - read_line->pointer_cursor) + 1)
	, term_capability, read_line, 1);
}

void	ft_insert_to_array(char **line, char buf, size_t pointer_last_symbol,
		size_t pointer_cursor, t_term **term_capability, t_read_line *read_line)
{
	size_t	i;
	size_t	i2;

	i = pointer_last_symbol;
	ft_memmove(&((*line)[pointer_cursor + 1]), &((*line)[pointer_cursor]),
			pointer_last_symbol - pointer_cursor);
	(*line)[pointer_cursor] = buf;

	ft_remove_tail_line(read_line, term_capability);
	ft_print(term_capability, read_line);
}

int		ft_vector(t_read_line *read_line, char buf, t_term **term_capability)
{
	if (!ft_check_and_expansion_array(&(read_line->line),
			read_line->pointer_last_symbol, &(read_line->length_line)))
		return (0);
	ft_insert_to_array(&(read_line->line), buf,
			read_line->pointer_last_symbol, read_line->pointer_cursor, term_capability, read_line);
	(read_line->pointer_last_symbol)++;
	(read_line->pointer_cursor)++;
	return (1);
}
