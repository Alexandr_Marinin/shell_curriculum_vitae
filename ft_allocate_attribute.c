/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_allocate_attribute.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 17:39:38 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/23 21:09:22 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_allocate_atribute_2(t_spisok **buf_node, t_spisok **node
	, struct dirent *entry)
{
	if (!(*buf_node = ft_allocate_t_spisok()))
	{
		ft_free_t_spisok(node);
		return (-1);
	}
	if (!((*buf_node)->name = ft_strdup(entry->d_name)))
	{
		free(*buf_node);
		ft_free_t_spisok(node);
		return (-1);
	}
	return (1);
}

int	ft_allocate_atribute_3(t_spisok **node, struct dirent *entry, char *path_way
	, int len_path)
{
	if (!((*node) = (t_spisok*)malloc(sizeof(t_spisok))))
		return (-1);
	if (!((*node)->name = ft_strdup(entry->d_name)))
	{
		free(*node);
		ft_free_t_spisok(node);
		return (-1);
	}
	if (!((*node)->content = ft_full_way_to_file(path_way, entry->d_name
		, len_path)))
	{
		free((*node)->name);
		free(*node);
		ft_free_t_spisok(node);
		return (-1);
	}
	(*node)->next = NULL;
	return (1);
}

int	ft_allocate_attribute_4(t_spisok **buf_node, t_spisok **node)
{
	free((*buf_node)->name);
	free((*buf_node));
	ft_free_t_spisok(node);
	return (-1);
}

int	ft_allocate_atribute(char *p_w, t_spisok **node)
{
	DIR				*directory;
	struct dirent	*e;
	t_spisok		*buf_node;
	int				len;

	if (!(p_w) || !(directory = opendir(p_w)))
		return (-1);
	len = ft_strlen(p_w);
	while ((e = readdir(directory)))
		if (!(*node))
		{
			if (!(ft_allocate_atribute_3(node, e, p_w, len)))
				return (-1);
		}
		else
		{
			if (!(ft_allocate_atribute_2(&buf_node, node, e)))
				return (-1);
			if (!(buf_node->content = ft_full_way_to_file(p_w, e->d_name, len)))
				return (ft_allocate_attribute_4(&buf_node, node));
			buf_node->next = *node;
			*node = buf_node;
		}
	closedir(directory);
	return (0);
}
