/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 16:09:54 by fwyman            #+#    #+#             */
/*   Updated: 2019/08/23 14:58:03 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*ft_del_spase(char **read_line)
{
	char	*buffer;
	size_t	i;
	size_t	i1;

	i = 0;
	i1 = 0;
	if (!(buffer = ft_strnew(ft_strlen(*read_line))))
		ft_write_error("malloc don't work!");
	while ((*read_line)[i])
		if ((((*read_line)[i] == ' ' || (*read_line)[i] == '\t')
		&& ((*read_line)[i + 1] == ' ' || (*read_line)[i + 1] == '\t'
		|| ((*read_line)[i + 1] == ';' || (i1 > 0 && buffer[i1 - 1] == ';'))))
		|| (((*read_line)[i] == ' ' || (*read_line)[i] == '\t') && !i1))
			i++;
		else
		{
			if ((*read_line)[i] == '\t')
				buffer[i1] = ' ';
			else
				buffer[i1] = (*read_line)[i];
			i++;
			i1++;
		}
	free(*read_line);
	return (*read_line = buffer);
}

void	ft_free_t_command(t_command **command_struct)
{
	int			i;
	t_command	*next_buf;

	i = 0;
	while (*command_struct)
	{
		next_buf = (*command_struct)->next;
		while ((*command_struct) && (*command_struct)->command
		&& (*command_struct)->command[i])
		{
			free((*command_struct)->command[i]);
			(*command_struct)->command[i] = NULL;
			i++;
		}
		i = 0;
		if ((*command_struct) && (*command_struct)->command)
		{
			free((*command_struct)->command);
			(*command_struct)->command = NULL;
		}
		if ((*command_struct))
			free(*command_struct);
		(*command_struct) = next_buf;
	}
}

void	ft_launch_readline_and_allocate_bor(t_bor **bor, char ***new_env,
		t_command **command_struct, int *fd, int *num_str)
{
	char			*read_line;
	char			*path;
	t_spisok		*spisok;
	struct termios	save_default;
	t_term			*term;

	path = ft_getenv("PATH", *new_env);
	spisok = ft_split_path(path);
	(*bor) = ft_in_bor(spisok);
	ft_free_t_spisok(&spisok);

	ft_terminfo(&term);

	tty_cbreak(0, &save_default);
	read_line = ft_read_line(bor, &term);
	write(1, "\n", 1);
	tty_reset(0, save_default);
	ft_in_config(read_line, num_str, *fd);
	if (!(read_line))
		ft_write_error("malloc don't work!");
	ft_del_spase(&read_line);
	ft_filling_all_list(command_struct, &read_line);
}

void	ft_launch_program(t_command **command_struct,
		t_bor **bor, char ***new_env)
{
	t_command		*command_struct_start;

	if (*command_struct)
	{
		command_struct_start = *command_struct;
		while (*command_struct)
		{
			ft_pre_run_program((*command_struct)->command[0],
					(*command_struct)->command, new_env, bor);
			(*command_struct) = (*command_struct)->next;
		}
		ft_free_t_command(&command_struct_start);
	}
}

int		main(int argc, char **argv, char **env)
{
	t_bor			*bor;
	t_command		*command_struct;
	char			**new_env;
	int				fd;
	int				num_str;

	(void)argc;
	(void)argv;
	num_str = 0;
	command_struct = NULL;
	fd = ft_config();
	if (!(new_env = ft_copy_env(env)))
		ft_write_error("error allocated memory in ft_copy_env");
	while (1)
	{
		ft_launch_readline_and_allocate_bor(&bor, &new_env, &command_struct, &fd, &num_str);
		ft_parc_struct(&command_struct, new_env);
		ft_launch_program(&command_struct, &bor, &new_env);
		ft_free_bor(&bor);
	}
	return (0);
}
