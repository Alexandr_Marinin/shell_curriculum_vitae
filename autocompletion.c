/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   autocompletion.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 15:40:25 by fwyman            #+#    #+#             */
/*   Updated: 2019/07/22 19:49:35 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_mid_words(char **line, size_t *pointer_last_symbol,
		size_t *pointer_cursor, t_read_line *read_line, t_term **term_capability)
{
	if (((*line)[*pointer_cursor] > 32 && (*line)[*pointer_cursor] != ';')
			&& (*pointer_cursor) < (*pointer_last_symbol))
	{
		while (((*line)[*pointer_cursor] > 32 && (*line)[*pointer_cursor]
					!= ';') && (*pointer_cursor) < (*pointer_last_symbol))
		{
			ft_move_right_cursor(read_line, term_capability);
		}
		return (1);
	}
	else
		return (0);
}

int		ft_command_or_attribute(char *line, int pointer_cursor)
{
	if (--pointer_cursor == -1 || line[pointer_cursor] == ';')
		return (0);
	else if (line[pointer_cursor] > 32)
	{
		while (pointer_cursor >= 0 && line[pointer_cursor] > 32
				&& line[pointer_cursor] != ';')
			pointer_cursor--;
		while (pointer_cursor >= 0 && line[pointer_cursor] <= 32)
			pointer_cursor--;
		if (pointer_cursor == -1 || line[pointer_cursor] == ';')
			return (1);
		else
			return (2);
	}
	else
	{
		while (line[pointer_cursor] <= 32 && pointer_cursor >= 0)
			pointer_cursor--;
		if (pointer_cursor == -1 || line[pointer_cursor] == ';')
			return (0);
		else
			return (2);
	}
}

int		ft_autocompletion(t_read_line *read_line, char *buf, t_bor **start_prog, t_term **term_capability)
{
	int		a;

	if (buf[0] != 9)
		return (0);
	if (ft_mid_words(&(read_line->line), &(read_line->pointer_last_symbol)
	, &(read_line->pointer_cursor), read_line, term_capability))
		return (1);
	else if (!(a = ft_command_or_attribute(read_line->line
		, read_line->pointer_cursor)))
		return (0);
	else if (a == 1 || a == 2)
	{
		return (ft_launch_print_autocompletion(read_line, term_capability, buf, start_prog, a));
	}
	return (0);
}
