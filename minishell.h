/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/26 19:59:04 by hewisp            #+#    #+#             */
/*   Updated: 2019/08/27 17:57:42 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H
# include "libft.h"
# include <termios.h>
# include <dirent.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <sys/stat.h>
# include <signal.h>
# include <fcntl.h>
# include <termcap.h>
# include <sys/ioctl.h>

# include <stdio.h>

typedef struct				s_parcer
{
	char					*buf;
	char					*buf_buf;
	char					symbol;
	int						i;
	int						j;
	int						len_buf;
	int						len_line;
}							t_parcer;

typedef struct				s_capability_names
{
	char					*left;
	char					*right;
	char					*move_left;
	char					*move_right;
	char					*up;
	char					*down;
	char					*move_start_down_row;
	char					*move_up_row;
}							t_term;

typedef struct				s_read_line
{
	char					*line;
	size_t					length_line;
	size_t					pointer_last_symbol;
	size_t					pointer_cursor;
	size_t					colums_cursor;
	size_t					line_cursor;
	size_t					pointer_start_you_string;
	unsigned short			ws_colums;
	size_t					prefix_size;
	size_t					max_column_cursor;
	size_t					max_line_cursor;
	t_term					*term_capability;
	// size_t					row_counter; //max_line
}							t_read_line;

typedef struct				s_print
{
	size_t					start_pos_curs;
	size_t					start_line_curs;
	size_t					finish_pos_curs;
	size_t					finish_line_curs;
	int						move_cursor;
}							t_print;

typedef struct				s_list_command
{
	struct s_list_command	*next;
	char					**command;
}							t_command;

typedef struct				s_spisok
{
	struct s_spisok			*next;
	char					*name;
	char					*content;
}							t_spisok;

typedef struct				s_bor
{
	int						ch;
	char					end;
	char					*full_way;
	struct s_bor			*right;
	struct s_bor			*down;
}							t_bor;

typedef struct				s_autodop
{
	struct s_autodop		*next;
	char					*full_way;
}							t_auto;

// typedef struct				s_capability_names
// {
// 	char					*left;
// 	char					*right;
// 	char					*move_left;
// 	char					*move_right;
// 	char					*up;
// 	char					*down;
// 	char					*move_start_down_row;
// 	char					*move_up_row;
// }							t_term;

void						ft_filling_all_list(t_command **command_struct
	, char **read_line);

int							tty_cbreak(int fd, struct termios *save_default);
int							tty_reset(int fd, struct termios save_default);
int							ft_check_programm(char *path);
/*
**parcer
*/
int							ft_parcing_path(char **line, t_parcer *st
	, char **env);
char						ft_count_symbol(char *line);
int							ft_insert_home(char **buf, int *j, char **env);

/*
** trie
*/
t_spisok					*ft_split_path(char *path);
t_bor						*ft_in_bor(t_spisok *start);
t_bor						*ft_search_entry(char *str, t_bor *start);
t_auto						*ft_print_bor(t_bor *start1, t_bor *start
	, t_auto **spisok);
int							ft_allocate_atribute(char *path_way
	, t_spisok **node);
t_auto						*ft_all_attribute(char *left_word, t_bor **bor);
int							ft_pre_run_program(char *path, char **argv
	, char ***env, t_bor **bor);

/*
** struct
*/
t_spisok					*ft_allocate_t_spisok(void);
void						ft_free_t_spisok(t_spisok **spisok);
t_bor						*ft_allocate_t_bor(void);

/*
** shell program
*/
int							ft_cd(char **argv, char **env);

/*
** autocompletion
*/
// int							ft_launch_print_autocompletion(
// 	t_read_line *read_line, int *buf, t_bor **start_prog, int a);
// int		ft_launch_print_autocompletion(t_read_line *read_line, char *buf,
// 		t_bor **start_prog, int a);
int		ft_launch_print_autocompletion(t_read_line *read_line, t_term **term_capability, char *buf,
		t_bor **start_prog, int a);
// int							ft_autocompletion(t_read_line *read_line
	// , int *buf, t_bor **start_prog);
// int		ft_autocompletion(t_read_line *read_line, char *buf, t_bor **start_prog);
int		ft_autocompletion(t_read_line *read_line, char *buf, t_bor **start_prog, t_term **term_capability);
// int							ft_print_autocompletion(t_read_line *read_line
// 	, int *buf, t_auto **arrg, size_t i_left_symbol_cursor);
// int		ft_print_autocompletion(t_read_line *read_line, char *buf, t_auto **arrg,
// 		size_t i_left_symbol_cursor);
int		ft_print_autocompletion(t_read_line *read_line, t_term **term_capability, char *buf, t_auto **arrg,
		size_t i_left_symbol_cursor);
void						ft_del_to_array(char **line
	, size_t pointer_last_symbol, size_t pointer_cursor
	, size_t count_del_symbol);

/*
** bor
*/
t_bor						*ft_allocate_bor(char *str, char *way
	, t_bor *start);
void						ft_free_bor(t_bor **bor);
t_auto						*ft_print_bor2(t_bor *start1, t_bor *start
	, t_auto **spisok);

void						ft_all_attribute_6(char *slash, t_bor **bor_buf
	, t_bor **bor
	, t_auto **spisok_auto);
void						ft_free_t_auto(t_auto **list);
t_auto						*ft_allocate_t_auto(void);
int							ft_path_allocate(char *path_way, t_spisok **node);
char						*ft_full_way_to_file(char *path_way, char *name
	, int len_path);

/*
** tab
*/
size_t						ft_paste_counter_colums(char *line
	, size_t pointer_colums_from, size_t pointer_before);

/*
** readline
*/
int							ft_check_and_expansion_array(char **line
	, size_t pointer_last_symbol, size_t *length_line);
char						*ft_getenv(char *path, char **env);
char						*ft_read_line(t_bor **bor, t_term **term_capability);
// void						ft_remove_to_array(char **line
// 	, size_t pointer_last_symbol, size_t pointer_cursor, size_t prefix_size);
void						ft_remove_to_array(t_read_line *read_line);//, t_term **term_capability);
int							ft_vector(t_read_line *read_line, char buf, t_term **term_capability);
void						ft_back_counter_colums(t_read_line *read_line, t_term **term_capability, t_print *print_struct);
// int							ft_remove_tail_line(t_read_line *read_line, t_term **term_capability);
// void						ft_print(t_term **term_capability, t_read_line *read_line);
void						ft_remove_tail_line(t_read_line *read_line, t_term **term_capability);
void						ft_print_text_multistring(char *line, size_t size_print, t_term **term_capability, t_read_line *read_line, int move_cursor);



/*
** move cursor
*/
void						ft_move_left_cursor(t_read_line *read_line, t_term **term_capability);
void						ft_move_right_cursor(t_read_line *read_line, t_term **term_capability);

/*
** lexer
*/
char						**ft_new_split(char *str);
size_t						ft_search_symbol_split(char *str);
int							ft_parc_struct(t_command **command, char **env);
void						ft_free_t_command(t_command **command_struct);

/*
** env
*/
char						*ft_free_array(char ***array);
char						**ft_copy_env(char **env);
char						**ft_setenv(char ***env, char **argv);
char						**ft_getpath(char *path, char **env);
char						*ft_allocate_path(char **env_path
	, char *path, char *arg);
void						ft_env(char **env);
int							ft_unsetenv(char **argv, char ***env);

/*
** error
*/
void						ft_write_error(char *str);

/*
** signal
*/
void	ft_signal(int signal);
void	ft_ignore_signal(int signal);

/*
** config
*/
int	ft_config(void);
int	ft_in_config(char *str, int *num_str, int fd);
char	*ft_search_config(int num_str, int *fd, char *name_dir);

//terminfo
void	ft_terminfo();

#endif
