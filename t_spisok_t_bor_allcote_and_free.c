/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_spisok_t_bor_allcote_and_free.c                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 17:39:24 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/26 17:15:48 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_spisok	*ft_allocate_t_spisok(void)
{
	t_spisok *spisok;

	if (!(spisok = (t_spisok*)malloc(sizeof(t_spisok))))
		return (NULL);
	spisok->next = NULL;
	spisok->content = NULL;
	spisok->name = NULL;
	return (spisok);
}

void		ft_free_t_spisok(t_spisok **spisok)
{
	t_spisok *spisok_buf;

	while (*spisok)
	{
		spisok_buf = (*spisok)->next;
		if ((*spisok)->name)
		{
			free((*spisok)->name);
			(*spisok)->name = NULL;
		}
		if ((*spisok)->content)
		{
			free((*spisok)->content);
			(*spisok)->content = NULL;
		}
		free(*spisok);
		(*spisok) = NULL;
		*spisok = spisok_buf;
	}
}

t_bor		*ft_allocate_t_bor(void)
{
	t_bor *bor;

	if (!(bor = (t_bor*)malloc(sizeof(t_bor))))
		return (NULL);
	bor->down = NULL;
	bor->right = NULL;
	bor->full_way = NULL;
	bor->end = 0;
	bor->ch = '\0';
	return (bor);
}

void		ft_free_bor(t_bor **bor)
{
	t_bor *bor_buff;

	bor_buff = (*bor);
	while (*bor)
	{
		if ((*bor)->right)
			ft_free_bor(&((*bor)->right));
		bor_buff = (*bor)->down;
		if ((*bor)->full_way)
		{
			free((*bor)->full_way);
			(*bor)->full_way = NULL;
		}
		free((*bor));
		(*bor) = NULL;
		(*bor) = bor_buff;
	}
}
