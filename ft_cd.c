/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 18:07:08 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/16 18:07:37 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_cd_2(char **argv, char **env)
{
	if (!(ft_strcmp("-", argv[1])))
	{
		if (chdir(ft_getenv("OLDPWD", env)))
			return (0);
		ft_putendl(ft_getenv("OLDPWD", env));
	}
	else
	{
		if (chdir(argv[1]))
		{
			ft_putstr("no such file or directory: ");
			ft_putendl(argv[1]);
			return (0);
		}
	}
	return (1);
}

int	ft_cd_3(char **argv, char **env)
{
	if (!argv[1])
	{
		if (chdir(ft_getenv("HOME", env)))
		{
			ft_putstr("no such file or directory: ");
			ft_putendl(argv[1]);
			return (0);
		}
	}
	else if (!(ft_cd_2(argv, env)))
		return (0);
	return (1);
}

int	ft_cd_4(char **buf_pwd)
{
	char dir[256];

	free(*buf_pwd);
	getcwd(dir, 256);
	if (!(ft_allocate_path(buf_pwd, "PWD", dir)))
		return (0);
	return (1);
}

int	ft_cd(char **argv, char **env)
{
	char **old_pwd;
	char *buf_old_pwd;
	char **buf_pwd;

	buf_old_pwd = ft_strdup(ft_getenv("PWD", env));
	buf_pwd = ft_getpath("PWD", env);
	if (!(ft_cd_3(argv, env)))
		return (0);
	if (buf_pwd && *buf_pwd)
	{
		if (!(ft_cd_4(buf_pwd)))
			return (0);
	}
	if (buf_old_pwd)
	{
		old_pwd = ft_getpath("OLDPWD", env);
		if (*old_pwd)
			free(*old_pwd);
		if (!(ft_allocate_path(old_pwd, "OLDPWD", buf_old_pwd)))
			return (0);
		if (buf_old_pwd)
			free(buf_old_pwd);
	}
	return (1);
}
