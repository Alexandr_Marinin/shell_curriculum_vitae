/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filling_structure.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 17:34:57 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/26 13:14:42 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

size_t	ft_search_symbol(char **read_line)
{
	size_t	i;

	i = 0;
	while ((*read_line)[i])
	{
		if ((*read_line)[i] == ';')
			return (i + 1);
		else if ((*read_line)[i] == '\'' || (*read_line)[i] == '"')
			i += ft_search_symbol_split(&((*read_line)[i]));
		else
			i++;
	}
	return (0);
}

char	*ft_parcing(char **read_line)
{
	char	*buffer;
	size_t	i;

	buffer = NULL;
	if (!(i = ft_search_symbol(read_line)))
		return (NULL);
	i--;
	if (!(buffer = strndup((*read_line), i)))
	{
		write(2, "malloc don't work\n", 19);
		exit(0);
	}
	*read_line += i + 1;
	return (buffer);
}

void	ft_filling_structure(t_command **command_struct, char *read_line)
{
	if (!((*command_struct) = malloc(sizeof(t_command)))
	|| !((*command_struct)->command = ft_new_split(read_line)))
	{
		write(2, "malloc don't work\n", 19);
		exit(0);
	}
	(*command_struct)->next = NULL;
}

void	ft_norme_filling_all_list(t_command **command_struct_start,
		t_command **command_buffer_next, char **read_line)
{
	if (!(*command_struct_start))
		ft_filling_structure(command_struct_start, (*read_line));
	else
		ft_filling_structure(command_buffer_next, (*read_line));
}

void	ft_filling_all_list(t_command **command_struct, char **read_line)
{
	char		*buffer;
	char		*read_line_start;
	t_command	*command_struct_start;
	t_command	*command_buffer;

	command_struct_start = NULL;
	read_line_start = *read_line;
	while ((buffer = ft_parcing(read_line)) != NULL)
	{
		if (!(command_struct_start))
		{
			ft_filling_structure(&command_struct_start, buffer);
			command_buffer = command_struct_start;
		}
		else
		{
			ft_filling_structure(&(command_buffer->next), buffer);
			command_buffer = command_buffer->next;
		}
		free(buffer);
	}
	ft_norme_filling_all_list(&command_struct_start, &(command_buffer->next),
			read_line);
	*command_struct = command_struct_start;
	free(read_line_start);
}
