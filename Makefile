NAME = minishell

FILES = main.c filling_structure.c tty_settings.c trie.c autocompletion.c \
new_split.c parcing.c read_line.c read_line_delete.c read_line_vector.c \
autocompletion_launch_print.c autocompletion_print.c \
t_spisok_t_bor_allcote_and_free.c ft_path_allocate.c \
ft_print_bor.c ft_allocate_bor.c ft_setenv.c mics.c env.c \
ft_allocate_attribute.c ft_all_attribute.c ft_all_attribute_2.c \
ft_cd.c ft_run_programm.c ft_run_programm_2.c ft_parcing_2.c error.c signal.c \
config.c terminfo.c read_line_print_multistring.c

OBJECTS = main.o filling_structure.o tty_settings.o trie.o autocompletion.o \
new_split.o parcing.o read_line.o read_line_delete.o read_line_vector.o \
autocompletion_launch_print.o autocompletion_print.o \
t_spisok_t_bor_allcote_and_free.o ft_path_allocate.o \
ft_print_bor.o ft_allocate_bor.o ft_setenv.o mics.o env.o \
ft_allocate_attribute.o ft_all_attribute.o ft_all_attribute_2.o \
ft_cd.o ft_run_programm.o ft_run_programm_2.o ft_parcing_2.o error.o signal.o \
config.o terminfo.o read_line_print_multistring.o

FLAGS = #-Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJECTS)
	@gcc $(FLAGS) $(OBJECTS) -o $(NAME) -L ./libft -lft -ltermcap -I ./libft/ -g
	@echo "Все файлы собраны!"
$(OBJECTS):
	@gcc $(FLAGS) -c $(FILES) -I ./libft/ -g
	@make -C ./libft/
	@echo "Объектные файлы собраны!"
clean:
	@rm -f $(OBJECTS)
	@make clean -C ./libft/
	@echo "Объектные файлы уничтожены!"
fclean: clean
	@rm -f $(NAME)
	@make fclean -C ./libft/
git: fclean
	make fclean -C ./libft/
re: fclean all
