/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:43:05 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 13:43:14 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	size_t	s_len;
	size_t	counter;
	char	*pointer;

	if (!s)
		return (NULL);
	counter = 0;
	s_len = ft_strlen(s) + 1;
	pointer = ft_memalloc(s_len);
	if (pointer == NULL)
		return (NULL);
	while (s[counter])
	{
		pointer[counter] = f(counter, s[counter]);
		counter++;
	}
	return (pointer);
}
