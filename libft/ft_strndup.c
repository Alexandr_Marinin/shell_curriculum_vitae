/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 16:53:55 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 17:26:15 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *src, size_t n)
{
	size_t	counter;
	char	*p;

	counter = 0;
	p = (char*)malloc(sizeof(char) * (n + 1));
	if (p == NULL)
		return (0);
	while (src[counter] != '\0' && counter < n)
	{
		p[counter] = src[counter];
		counter++;
	}
	p[counter] = '\0';
	return (p);
}
