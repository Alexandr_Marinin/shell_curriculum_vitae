/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 14:04:37 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 14:04:51 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	start;
	size_t	finish;
	size_t	size;
	char	*pointer;

	pointer = NULL;
	if (s)
	{
		start = 0;
		finish = ft_strlen(s) - 1;
		while (s[start] == ' ' || s[start] == '\n' || s[start] == '\t')
			start++;
		while ((s[finish] == ' ' || s[finish] == '\n' || s[finish] == '\t')
		&& finish > start)
			finish--;
		size = finish - start + 1;
		pointer = ft_strsub(s, start, size);
	}
	return (pointer);
}
