/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:16:14 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 19:24:05 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *buf, int ch, size_t count)
{
	size_t			counter;
	unsigned char	*p;

	counter = 0;
	p = (unsigned char*)buf;
	while (counter < count)
	{
		p[counter] = (unsigned char)ch;
		counter++;
	}
	return (buf);
}
