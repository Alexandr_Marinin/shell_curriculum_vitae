/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 21:00:34 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/09 21:01:39 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *new;
	t_list *exit;

	if (lst && f)
	{
		new = f(lst);
		exit = new;
		while (lst->next)
		{
			lst = lst->next;
			new->next = f(lst);
			if (!(new->next))
			{
				free(lst);
				return (NULL);
			}
			new = new->next;
		}
		return (exit);
	}
	return (NULL);
}
