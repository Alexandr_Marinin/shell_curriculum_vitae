/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:39:26 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 13:39:54 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	s1_len;
	size_t	s2_len;
	char	*pointer;

	pointer = NULL;
	if (s1 && s2)
	{
		s1_len = ft_strlen(s1);
		s2_len = ft_strlen(s2);
		pointer = ft_strnew(s1_len + s2_len);
		if (pointer == NULL)
			return (NULL);
		ft_strcpy(pointer, s1);
		ft_strcpy(&pointer[s1_len], s2);
		return (pointer);
	}
	return (pointer);
}
