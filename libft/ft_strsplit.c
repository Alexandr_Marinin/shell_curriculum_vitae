/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 14:02:37 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 17:21:41 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	size_t		ft_len_plus(char const *s, char c)
{
	size_t	len;
	size_t	counter;

	len = 0;
	counter = 0;
	while (s[counter] && s[counter] != c)
	{
		len++;
		counter++;
	}
	return (len);
}

static	char		**ft_word_copying(char const *s, char **pointer, char c)
{
	size_t	counter;
	size_t	counter_array;

	counter_array = 0;
	counter = 0;
	while (s[counter])
		if (s[counter] == c)
			counter++;
		else
		{
			pointer[counter_array] = ft_strsub(s, counter,
					ft_len_plus(&s[counter], c));
			if (pointer[counter_array] == NULL)
			{
				ft_strdel_all_array(pointer, counter_array);
				return (NULL);
			}
			while (s[counter] && s[counter] != c)
				counter++;
			counter_array++;
		}
	return (pointer);
}

static	char		**ft_memory_allocation(char const *s, char c)
{
	char	**pointer;
	size_t	counter;
	size_t	counter_pointer;

	counter = 0;
	counter_pointer = 0;
	while (s[counter])
	{
		if (s[counter] == c)
			counter++;
		else
		{
			counter_pointer++;
			while (s[counter] && s[counter] != c)
				counter++;
		}
	}
	pointer = (char**)malloc(sizeof(char *) * (counter_pointer + 1));
	if (pointer == NULL)
		return (NULL);
	pointer[counter_pointer] = NULL;
	return (pointer);
}

char				**ft_strsplit(char const *s, char c)
{
	char	**pointer;

	pointer = NULL;
	if (s)
	{
		pointer = ft_memory_allocation(s, c);
		if (pointer == NULL)
			return (NULL);
		pointer = ft_word_copying(s, pointer, c);
		if (pointer == NULL)
			return (NULL);
	}
	return (pointer);
}
