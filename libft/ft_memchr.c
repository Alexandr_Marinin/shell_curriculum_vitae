/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 19:12:48 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 19:21:33 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t				counter;
	unsigned const char	*a;
	void				*pointer;

	pointer = (void *)s;
	a = (unsigned const char*)s;
	counter = 0;
	while (counter < n)
	{
		if (a[counter] == (unsigned char)c)
		{
			pointer = (void *)(s + counter);
			return (pointer);
		}
		else
		{
			counter++;
		}
	}
	pointer = NULL;
	return (pointer);
}
