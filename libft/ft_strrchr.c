/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:48:19 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/11 19:35:09 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		counter;

	counter = ft_strlen((char *)s);
	while (counter > 0 && s[counter] != (char)c)
	{
		counter--;
	}
	if (s[counter] == (char)c)
		return ((char *)&s[counter]);
	return (NULL);
}
