/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:42:34 by fwyman            #+#    #+#             */
/*   Updated: 2019/07/04 16:33:59 by aleksei          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlen(const char *s)
{
	size_t counter;

	if (!s || !(*s))
		return (0);
	counter = 0;
	while (s[counter])
		counter++;
	return (counter);
}
