/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:11:22 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 19:22:16 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t				counter;
	unsigned const char	*a;
	unsigned const char	*b;

	counter = 0;
	a = (unsigned const char*)s1;
	b = (unsigned const char*)s2;
	while (counter < n)
	{
		if (a[counter] != b[counter])
			return (a[counter] - b[counter]);
		else
		{
			counter++;
		}
	}
	return (0);
}
