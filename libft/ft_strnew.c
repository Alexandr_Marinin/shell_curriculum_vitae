/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:47:46 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 18:50:12 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*pointer;
	size_t	x;

	x = 0;
	if (size == x - 1)
		return (NULL);
	pointer = ft_memalloc(size + 1);
	return (pointer);
}
