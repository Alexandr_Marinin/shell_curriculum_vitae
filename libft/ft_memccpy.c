/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:08:49 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 19:20:40 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *destination, const void *source, int c, size_t n)
{
	size_t				counter;
	unsigned char		*a;
	unsigned const char	*b;
	unsigned char		c1;

	counter = 0;
	c1 = (unsigned char)c;
	a = (unsigned char*)destination;
	b = (unsigned const char*)source;
	while (counter < n)
	{
		a[counter] = b[counter];
		if (b[counter] == c1)
		{
			counter++;
			return (destination + counter);
		}
		counter++;
	}
	return (NULL);
}
