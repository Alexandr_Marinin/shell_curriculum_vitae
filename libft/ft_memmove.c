/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:15:36 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 19:23:34 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *destptr, const void *srcptr, size_t num)
{
	size_t				counter;
	unsigned char		*a;
	unsigned const char	*b;

	counter = 0;
	a = (unsigned char*)destptr;
	b = (unsigned const char*)srcptr;
	if (b < a)
	{
		while (num--)
		{
			a[num] = b[num];
		}
		return (destptr);
	}
	while (counter < num)
	{
		a[counter] = b[counter];
		counter++;
	}
	return (destptr);
}
