/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 19:37:09 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/11 14:19:11 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_atoi(const char *string)
{
	unsigned long int	cou;
	long long int		a;
	long long int		b;
	int					index;

	cou = 0;
	index = 1;
	a = 0;
	while (string[cou] == '\v' || string[cou] == '\f' || string[cou] == '\r' ||
	string[cou] == '\t' || string[cou] == '\n' || string[cou] == ' ')
		cou++;
	if (string[cou] == '-' || string[cou] == '+')
	{
		if (string[cou] == '-')
			index = -1;
		cou++;
	}
	while (string[cou] >= '0' && string[cou] <= '9')
	{
		b = a;
		a = a * 10 + (index * (string[cou++] - '0'));
		if ((b > 0 && a < 0) || (b < 0 && a > 0))
			return ((b > 0 && a < 0) ? -1 : 0);
	}
	return ((int)a);
}
