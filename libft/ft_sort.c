/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 18:16:02 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 18:16:07 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_sort(char **a, int n)
{
	size_t	index;
	char	*p;

	index = 1;
	while ((int)index < n)
	{
		if (ft_strcmp(a[index], a[index + 1]) > 0)
		{
			p = a[index];
			a[index] = a[index + 1];
			a[index + 1] = p;
			index--;
		}
		else
			index++;
	}
}
