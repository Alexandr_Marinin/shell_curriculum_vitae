/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:36:54 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 13:36:58 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *s1, const char *s2)
{
	unsigned long int counter;
	unsigned long int counter1;

	counter = 0;
	counter1 = 0;
	while (s1[counter])
		counter++;
	while (s2[counter1])
	{
		s1[counter] = s2[counter1];
		counter++;
		counter1++;
	}
	s1[counter] = '\0';
	return (s1);
}
