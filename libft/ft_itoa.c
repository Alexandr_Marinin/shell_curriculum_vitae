/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 20:51:27 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 14:11:46 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int	ft_counter(int n)
{
	int n1;
	int len_counter;

	n1 = n;
	len_counter = 0;
	if (n == 0)
		return (1);
	if (n < 0)
	{
		n1 = n1 * (-1);
		len_counter++;
	}
	while (n1 > 0)
	{
		n1 = n1 / 10;
		len_counter++;
	}
	return (len_counter);
}

char		*ft_itoa(int n)
{
	char	*string;
	int		len_counter;

	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	len_counter = ft_counter(n);
	string = ft_strnew(len_counter--);
	if (string == NULL)
		return (NULL);
	if (n == 0)
		string[0] = '0';
	if (n < 0)
	{
		n = n * (-1);
		string[0] = '-';
	}
	while (n != 0)
	{
		string[len_counter--] = (n % 10) + '0';
		n = n / 10;
	}
	return (string);
}
