/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 20:42:07 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 19:16:26 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_bzero(void *b, size_t length)
{
	size_t	counter;
	char	*p;

	counter = 0;
	p = (char*)b;
	while (counter < length)
	{
		((char*)p)[counter] = '\0';
		counter++;
	}
}
