/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:38:11 by fwyman            #+#    #+#             */
/*   Updated: 2019/07/16 16:42:41 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *src)
{
	int		counter;
	char	*p;

	counter = 0;
	if (!src)
		return (NULL);
	while (src[counter] != '\0')
		counter++;
	if (!(p = (char*)malloc(sizeof(char) * (counter + 1))))
		return (NULL);
	counter = 0;
	while (src[counter] != '\0')
	{
		p[counter] = src[counter];
		counter++;
	}
	p[counter] = '\0';
	return (p);
}
