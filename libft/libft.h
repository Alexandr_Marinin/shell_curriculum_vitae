/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 17:52:37 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 20:08:06 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <string.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

void				*ft_memset (void *destination, int c, size_t n);
int					ft_atoi(const char *str);
size_t				ft_strlen(const char *string);
char				*ft_strdup(const char *str);
void				ft_bzero(void *b, size_t length);
char				*ft_strncpy(char *destination, const char *source,
	size_t n);
char				*ft_strcat (char *destination, const char *append);
int					ft_tolower(int c);
char				*ft_strncat(char *destptr, const char *srcptr, size_t num);
void				*ft_memccpy(void *dest, const void *source, int ch,
	size_t count);
void				*ft_memcpy(void *destptr, const void *srcptr, size_t num);
char				*ft_strcpy(char *destination, const char *source);
int					ft_isprint(int ch);
int					ft_isascii(int ch);
int					ft_isalnum(int ch);
int					ft_isdigit(int ch);
int					ft_isalpha(int ch);
void				*ft_memmove (void *destination, const void *source,
	size_t n);
char				*ft_strnstr(const char *haystack, const char *needle,
	size_t len);
char				*ft_strchr(const char *str, int ch);
char				*ft_strrchr(const char *s, int c);
int					ft_strncmp(const char *str1, const char *str2, size_t n);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
size_t				ft_strlcat(char *dest, const char *src, size_t size);
char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void(*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char(*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int					ft_strequ(char const *s1, char const*s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
void				ft_putnbr(int n);
void				ft_putchar(char c);
char				**ft_strsplit(char const *s, char c);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putnbr(int n);
void				ft_putstr(char const *s);
void				ft_putchar(char c);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putendl(char const *s);
t_list				*ft_lstnew(void const *content, size_t content_size);
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_putnbr_fd(int n, int fd);
int					ft_strcmp(const char *str1, const char *str2);
char				*ft_itoa(int n);
int					ft_toupper(int c);
char				*ft_strstr(const char *haystack, const char *needle);
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				*ft_binary_search(const long long int *list,
	long long int c, size_t len);
char				*ft_strndup(const char *src, size_t n);
void				ft_strdel_all_array(char **a, size_t i);
int					ft_min_int(int a, int b);
int					ft_max_int(int a, int b);
void				ft_swap(int *a, int *b);
void				ft_sort(char **a, int n);
int					ft_sqrt(int nb);

#endif
