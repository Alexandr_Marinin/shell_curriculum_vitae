/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 20:59:06 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 14:10:59 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *pointer;
	t_list *counter;

	if (alst && del)
	{
		pointer = *alst;
		while (pointer)
		{
			counter = pointer->next;
			del((pointer)->content, (pointer)->content_size);
			free(pointer);
			pointer = counter;
		}
		(*alst) = NULL;
	}
}
