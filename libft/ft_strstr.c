/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 17:06:06 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/11 20:38:19 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	int counter;

	counter = 0;
	if (!needle[0])
		return ((char *)haystack);
	while (haystack[counter])
	{
		if (ft_strncmp(&haystack[counter], needle, ft_strlen(needle)) == 0)
			return ((char*)&haystack[counter]);
		counter++;
	}
	return (NULL);
}
