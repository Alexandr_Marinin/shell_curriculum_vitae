/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_binary_search.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 20:20:28 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 18:25:08 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_binary_search(const long long int *list, long long int c,
	size_t len)
{
	size_t	low;
	size_t	mid;
	size_t	hight;
	int		*pointer;

	low = 0;
	pointer = NULL;
	hight = len - 1;
	while (low <= hight)
	{
		mid = (low + hight) / 2;
		if (list[mid] == c)
			return (pointer = (void*)&list[mid]);
		if (list[mid] > c)
			hight = mid - 1;
		else
			low = mid + 1;
	}
	return (pointer);
}
