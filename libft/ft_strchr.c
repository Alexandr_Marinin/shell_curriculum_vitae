/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:37:12 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/11 16:49:47 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	int counter;

	counter = 0;
	while (s[counter] && s[counter] != (char)c)
	{
		counter++;
	}
	if (s[counter] == (char)c)
		return ((char *)&s[counter]);
	return (NULL);
}
