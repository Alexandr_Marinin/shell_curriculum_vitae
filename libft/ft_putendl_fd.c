/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:19:37 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 13:19:43 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putendl_fd(char const *s, int fd)
{
	int counter;

	if (s)
	{
		counter = 0;
		while (s[counter])
		{
			ft_putchar_fd(s[counter], fd);
			counter++;
		}
		ft_putchar_fd('\n', fd);
	}
}
