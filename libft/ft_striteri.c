/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:39:13 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/12 16:13:58 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	unsigned int counter;

	counter = 0;
	if (s && f)
	{
		while (s[counter])
		{
			f(counter, &s[counter]);
			counter++;
		}
	}
}
