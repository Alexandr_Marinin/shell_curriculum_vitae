/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 21:03:40 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/11 18:47:12 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	void *pointer;

	pointer = (void*)malloc(size);
	if (pointer == NULL)
		return (NULL);
	ft_bzero(pointer, size);
	return (pointer);
}
