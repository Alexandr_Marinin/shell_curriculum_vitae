/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:48:10 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/12 16:27:31 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	counter;
	int		counter1;

	counter = -1;
	counter1 = 0;
	if (!needle[counter1])
		return ((char *)haystack);
	while (haystack[++counter] && counter < len)
		if (haystack[counter] == needle[counter1])
		{
			while (haystack[counter++] == needle[counter1++] && counter < len)
			{
				if (needle[counter1] == '\0' || counter > len)
					return ((char*)&(haystack[counter - counter1]));
			}
			if (needle[counter1] == '\0' && (haystack[counter - 1] ==
				needle[counter1 - 1]))
				return ((char*)&haystack[counter - counter1]);
			counter = counter - counter1;
			counter1 = 0;
		}
	return (NULL);
}
