/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:37:27 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 13:37:29 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_strclr(char *s)
{
	size_t	counter;

	counter = 0;
	if (s == NULL)
		return ;
	while (s[counter])
	{
		s[counter] = '\0';
		counter++;
	}
}
