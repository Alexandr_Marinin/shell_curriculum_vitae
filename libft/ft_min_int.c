/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_min_int.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 17:43:46 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 17:58:54 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_min_int(int a, int b)
{
	return ((a < b) ? a : b);
}
