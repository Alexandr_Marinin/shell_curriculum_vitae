/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 18:20:39 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 18:20:42 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	long int max;
	long int mid;
	long int min;

	min = 1;
	max = nb / 2;
	if (nb < 0)
		return (0);
	while (min <= max)
	{
		mid = (min + max) / 2;
		if (mid * mid == nb)
			return (mid);
		else if (mid * mid > nb)
			max = mid - 1;
		else
			min = mid + 1;
	}
	return (0);
}
