/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 14:04:18 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 14:04:27 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*pointer;

	pointer = NULL;
	if (s)
	{
		pointer = ft_strnew(len);
		if (pointer == NULL)
			return (NULL);
		ft_strncpy(pointer, &s[start], len);
	}
	return (pointer);
}
