/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:19:49 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/10 13:19:55 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putendl(char const *s)
{
	int	counter;

	if (s)
	{
		counter = 0;
		while (s[counter])
		{
			ft_putchar(s[counter]);
			counter++;
		}
		ft_putchar('\n');
	}
}
