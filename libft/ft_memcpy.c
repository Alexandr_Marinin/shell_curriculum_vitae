/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 18:50:16 by fwyman            #+#    #+#             */
/*   Updated: 2018/12/13 19:22:59 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *destptr, const void *srcptr, size_t num)
{
	size_t				counter;
	char				*a;
	char const			*b;

	counter = 0;
	a = (char*)destptr;
	b = (char const*)srcptr;
	if (num == 0 || destptr == srcptr)
		return (destptr);
	while (counter < num)
	{
		a[counter] = b[counter];
		counter++;
	}
	return (destptr);
}
