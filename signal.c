/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/26 20:54:30 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/26 20:58:47 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_signal(int signal)
{
	if (signal == SIGINT)
		ft_putstr("\n");
}

void	ft_ignore_signal(int signal)
{
	(void)signal;
}
