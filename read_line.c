/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 17:05:22 by hewisp            #+#    #+#             */
/*   Updated: 2019/08/26 17:51:11 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_ascii_keys(t_read_line *read_line/*, t_term **term_capability*/, char *buf)
{
	if (buf[0] == 127)
	{
		if ((read_line->pointer_cursor) > 0)
		{
			ft_remove_to_array(read_line);
		}
		return (1);
	}
	else
		return (0);
}

void	ft_move_left_cursor(t_read_line *read_line, t_term **term_capability)
{
	t_print		print_struct;

	print_struct.finish_pos_curs = read_line->colums_cursor;
	print_struct.finish_line_curs = read_line->line_cursor;
	if (read_line->colums_cursor)
	{
		(read_line->pointer_cursor)--;
		(read_line->colums_cursor)--;
	}
	else
	{
		read_line->colums_cursor = (read_line->ws_colums) - 1;
		(read_line->line_cursor)--;
	}
	print_struct.start_pos_curs = read_line->colums_cursor;
	print_struct.start_line_curs = read_line->line_cursor;
	ft_back_counter_colums(read_line, term_capability, &print_struct);
}

void	ft_move_right_cursor(t_read_line *read_line, t_term **term_capability)
{
	if (read_line->colums_cursor < (read_line->ws_colums) - 1)
	{
		(read_line->pointer_cursor)++;
		(read_line->colums_cursor)++;
		write(1, (*term_capability)->move_right, ft_strlen((*term_capability)->move_right));
	}
	else
	{
		read_line->colums_cursor = 0;
		(read_line->line_cursor)++;
		write(1, "\n\r", 2);
	}
}

int		ft_extended_keys(char *line, char *buf, size_t *pointer_last_symbol,
		size_t *pointer_cursor, t_term **term_capability, size_t i_bytes, size_t prefix_size, t_read_line *read_line)
{
	if (buf[0] == 27)
	{
		if (!(ft_memcmp((*term_capability)->left, buf, i_bytes)) && ((*pointer_cursor) > 0))
		{
			ft_move_left_cursor(read_line, term_capability);
		}
		else if (!(ft_memcmp((*term_capability)->right, buf, i_bytes)) && ((*pointer_cursor) < (*pointer_last_symbol)))
		{
			ft_move_right_cursor(read_line, term_capability);
		}
		return (1);
	}
	else
		return (0);
}

unsigned short	ft_window_size_colums(void)
{
	struct winsize	size;

	ioctl(0, TIOCGWINSZ, &size);
	return (size.ws_col);
}

int		ft_initialization_read_line(t_read_line *read_line, t_term **term_capability)
{
	write(1, "minishell:", 10);
	read_line->pointer_last_symbol = 0;
	read_line->pointer_cursor = 0;
	read_line->length_line = 128;
	read_line->ws_colums = ft_window_size_colums();
	read_line->prefix_size = 10;
	read_line->pointer_start_you_string = 0;
	read_line->colums_cursor = read_line->prefix_size;
	read_line->line_cursor = 0;
	read_line->max_column_cursor = read_line->prefix_size;
	read_line->max_line_cursor = 0;
	read_line->term_capability = *term_capability;
	if (!(read_line->line = ft_strnew(128)))
	{
		write(2, "malloc don't work\n", 19);
		return (0);
	}
	return (1);
}

char	*ft_read_line(t_bor **bor, t_term **term_capability)
{
	char	buf[10];
	t_read_line	read_line;
	size_t i_bytes = 0;

	if (!ft_initialization_read_line(&read_line, term_capability))
		return (NULL);
	ft_bzero(buf, 10);
	read_line.colums_cursor = read_line.prefix_size;
	while ((i_bytes = read(0, &buf, 10)))
	{
		signal(SIGINT, ft_ignore_signal);
		if (ft_autocompletion(&read_line, buf, bor, term_capability))
			continue;
		else if (buf[0] == '\n')
		{
			read_line.line[read_line.pointer_last_symbol] = '\0';
			return (read_line.line);
		}
		else if (ft_ascii_keys(&read_line/*, term_capability*/, buf))
				//&(read_line.pointer_last_symbol), &(read_line.pointer_cursor), read_line.prefix_size))
			;
		else if (ft_extended_keys(read_line.line, buf,
				&(read_line.pointer_last_symbol), &(read_line.pointer_cursor), term_capability, i_bytes, read_line.prefix_size, &read_line))
			;
		else
		{
			if (!ft_vector(&read_line, buf[0], term_capability))
				return (NULL);
		}
		ft_bzero(buf, 10);
	}
	return (read_line.line);
}
