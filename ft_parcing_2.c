/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parcing_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/26 16:55:04 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/26 17:04:11 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_parcing_path_2(char **buf_buf_path, char **buf_path, char **buf
	, char **env)
{
	if (!((*buf_buf_path) = ft_getenv((*buf_path), env)))
	{
		if (!((*buf) = ft_strjoin((*buf), "")))
		{
			free(*buf_path);
			return (0);
		}
	}
	else
	{
		if (!((*buf) = ft_strjoin((*buf), (*buf_buf_path))))
		{
			free(*buf_path);
			free(*buf_buf_path);
			return (0);
		}
	}
	return (1);
}

void	ft_parcing_path_3(char **line, t_parcer *st, int *a)
{
	while (((*line)[(st->i)] >= 'a' && (*line)[(st->i)] <= 'z')
	|| ((*line)[(st->i)] >= 'A' && (*line)[(st->i)] <= 'Z')
	|| ((*line)[(st->i)] >= '0' && (*line)[(st->i)] <= '9'))
	{
		(st->i) += 1;
		*a += 1;
	}
}

int		ft_parcing_path(char **line, t_parcer *st, char **env)
{
	int		a;
	char	*buf_path;
	char	*buf_buf_path;
	char	*buf_buf;

	a = 0;
	(st->i)++;
	ft_parcing_path_3(line, st, &a);
	if (a == 0)
	{
		(st->i) -= 1;
		return (1);
	}
	if (!(buf_path = ft_strndup(&(*line)[(st->i) - a], a)))
		return (0);
	buf_buf = (st->buf);
	if (!(ft_parcing_path_2(&buf_buf_path, &buf_path, &st->buf, env)))
		return (0);
	(st->j) += ft_strlen(buf_buf_path);
	free(buf_path);
	free(buf_buf);
	return (1);
}

char	ft_count_symbol(char *line)
{
	int		i;
	int		counter;
	char	symbol;

	i = 0;
	counter = 0;
	if (line[0] == '\"' || line[0] == '\'')
	{
		symbol = line[0];
	}
	else
		return ('!');
	while (line[i])
	{
		if (line[i] == symbol)
			counter++;
		i++;
	}
	if (counter % 2 == 0)
		return (symbol);
	else
		return ('\0');
}

int		ft_insert_home(char **buf, int *j, char **env)
{
	char	*buf_buf_path;
	char	*buf_buf;

	buf_buf = *buf;
	if (!(buf_buf_path = ft_getenv("HOME", env)))
	{
		if (!((*buf) = ft_strjoin((*buf), "")))
		{
			(*buf) = buf_buf;
			return (0);
		}
	}
	else
	{
		if (!((*buf) = ft_strjoin((*buf), buf_buf_path)))
		{
			(*buf) = buf_buf;
			return (0);
		}
	}
	(*j) = ft_strlen(*buf);
	free(buf_buf);
	return (1);
}
