/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_all_attribute_2.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 18:03:53 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/16 18:04:04 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_all_attribute_6(char *slash, t_bor **bor_buf, t_bor **bor
	, t_auto **spisok_auto)
{
	if (*(slash + 1))
	{
		*bor_buf = ft_search_entry(slash + 1, *bor);
		*spisok_auto = ft_print_bor(*bor_buf, *bor_buf, spisok_auto);
	}
	else
		*spisok_auto = ft_print_bor2(*bor, *bor, spisok_auto);
}
