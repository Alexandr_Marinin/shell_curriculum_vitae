/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trie.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 18:13:23 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/16 18:15:13 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_bor	*ft_in_bor(t_spisok *start)
{
	t_spisok	*list;
	t_bor		*bor;

	bor = NULL;
	if (!(list = start))
		return (NULL);
	while (list)
	{
		if (!(bor = ft_allocate_bor(list->name, list->content, bor)))
			return (NULL);
		list = list->next;
	}
	return (bor);
}

int		ft_search_entry_2(t_bor **zveno)
{
	if ((*zveno)->right)
		(*zveno) = (*zveno)->right;
	else
		return (0);
	return (1);
}

t_bor	*ft_search_entry(char *str, t_bor *start)
{
	int		i;
	t_bor	*zveno;

	i = 0;
	if (!(zveno = start))
		return (NULL);
	while (str[i])
		if (str[i] == zveno->ch)
			if (str[i + 1])
				if (zveno->down)
				{
					i++;
					zveno = zveno->down;
				}
				else
					return (NULL);
			else
				return (zveno);
		else
		{
			if (!(ft_search_entry_2(&zveno)))
				return (NULL);
		}
	return (NULL);
}

char	*ft_getenv(char *path, char **env)
{
	size_t	i;
	size_t	len_path;

	i = 0;
	len_path = ft_strlen(path);
	if (!(env) || !(path))
		return (NULL);
	while (env[i])
	{
		if (!(ft_strncmp(env[i], path, len_path)) && env[i][len_path] == '=')
			return (&(env[i][len_path + 1]));
		i++;
	}
	return (NULL);
}
