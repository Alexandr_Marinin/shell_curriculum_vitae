/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   config.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aleksei <aleksei@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/27 14:03:13 by aleksei           #+#    #+#             */
/*   Updated: 2019/07/28 17:09:52 by aleksei          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "minishell.h"

int	ft_config(void)
{
	int fd;

	if((fd = open("config", O_CREAT | O_APPEND | O_RDWR | O_TRUNC, S_IREAD | S_IWRITE)) == -1)
		return (-1);
	return (fd);
}

int ft_search_num(int fd)
{
	char	buf_str[100];
	char	buf_char;
	int		i;
	int		x;
	int		num;

	i = 0;
	buf_char = '\0';
	while (buf_char != '$' && buf_char != '\"')
	{
		if ((x = read(fd, &buf_char, 1)) <= 0)
			return (x == 0 ? -2 : -1);
		if(buf_char != '$' && buf_char != '\"')
		{
			buf_str[i] = buf_char;
			i++;
		}
	}
	buf_str[i] = '\0';
	num = ft_atoi(buf_str);
	return (num);
}

int	ft_skip_string(int fd)
{
	char	buf[100];
	int		num;

	num = -2;
	if ((num = ft_search_num(fd)) < 0)
		return (num);
	num += 1;
	if (num <= 100)
		read(fd, buf, num);
	else
	{
		while (num > 100)
		{
			read(fd, buf, 100);
			num -= 100;
		}
		if (num > 0)
			read(fd, buf, num);
	}
	return (1);
}

char *ft_config_in_array(int *fd)
{
	int		num;
	char	*str;

	num = -3;
	if ((num = ft_search_num(*fd)) < 0)
		return (NULL);
	if (!(str = (char*)malloc(sizeof(char) * (num + 1))))
		ft_write_error("malloc don't work in config");
	if ((read(*fd, str, num)) < 0)
		return (NULL);
	str[num] = '\0';
	return (str);
}

char	*ft_search_config(int num_str, int *fd, char *name_dir)
{
	int num;
	int	x;

	num = -2;
	if (num_str < 0)
		return (NULL);
	close(*fd);
	if((*fd = open("config", O_APPEND | O_RDWR, S_IREAD | S_IWRITE)) == -1)
		return (NULL);
	while (num_str != num)
	{
		if ((num = ft_search_num(*fd)) < 0)
		{
			close(*fd);
			if((*fd = open("config", O_APPEND | O_RDWR, S_IREAD | S_IWRITE)) == -1)
				return (NULL);
			return (NULL);
		}
		if (num == num_str)
			break;
		else
			ft_skip_string(*fd);
	}
	return (ft_config_in_array(fd));
}

int	ft_in_config(char *str, int *num_str, int fd)
{
	if (!(str) || fd == -1)
		return (0);
	ft_putnbr_fd(*num_str, fd);
	ft_putchar_fd('$', fd);
	ft_putnbr_fd(ft_strlen(str), fd);
	ft_putchar_fd('\"', fd);
	ft_putstr_fd(str, fd);
	ft_putchar_fd('\"', fd);
	(*num_str)++;
	return (1);
}
