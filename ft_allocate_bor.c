/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_allocate_bor.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 18:26:12 by hewisp            #+#    #+#             */
/*   Updated: 2019/07/15 18:38:04 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_allocate_bor_2(char *str, int i, t_bor **list, t_bor **start)
{
	if ((*list)->down)
	{
		if (str[i])
			(*list) = (*list)->down;
	}
	else
	{
		if (!((*list)->down = ft_allocate_t_bor()))
		{
			ft_free_bor(start);
			return (0);
		}
		(*list)->down->ch = str[i];
		(*list) = (*list)->down;
	}
	return (1);
}

int		ft_allocate_bor_3(char *str, t_bor **list, int i, t_bor **start)
{
	while (((*list)->ch != str[i]) && ((*list)->right))
		(*list) = (*list)->right;
	if ((*list)->ch != str[i])
	{
		if (!((*list)->right = ft_allocate_t_bor()))
		{
			ft_free_bor(start);
			return (0);
		}
		(*list)->right->ch = str[i];
		(*list) = (*list)->right;
	}
	return (1);
}

int		ft_allocate_bor_5(t_bor **start, t_bor **list, char *str, int i)
{
	if (!((*list) = (*start)))
	{
		if (!((*start) = ft_allocate_t_bor()))
		{
			ft_free_bor(start);
			return (0);
		}
		(*start)->ch = str[i];
		(*list) = (*start);
	}
	return (1);
}

int		ft_allocate_bor_4(t_bor **list, t_bor **start, int i, char *str)
{
	if (!(ft_allocate_bor_5(start, list, str, i)))
		return (0);
	while (str[i])
	{
		if ((*list)->ch == str[i])
		{
			i++;
			if (!str[i])
				break ;
			if (!(ft_allocate_bor_2(str, i, list, start)))
				return (0);
		}
		else
		{
			if (!(ft_allocate_bor_3(str, list, i, start)))
				return (0);
		}
	}
	return (1);
}

t_bor	*ft_allocate_bor(char *str, char *way, t_bor *start)
{
	t_bor	*list;
	int		i;

	i = 0;
	if (!str || !(*str) || !way)
		return (NULL);
	if (!(ft_allocate_bor_4(&list, &start, i, str)))
		return (NULL);
	if (!(list->full_way = ft_strdup(way)))
	{
		ft_free_bor(&start);
		return (NULL);
	}
	list->end = 1;
	return (start);
}
