/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tty_settings.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fwyman <fwyman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 18:04:35 by fwyman            #+#    #+#             */
/*   Updated: 2019/08/09 16:21:42 by fwyman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		tty_cbreak(int fd, struct termios *save_default)
{
	struct termios	buffer;

	if (tcgetattr(fd, &buffer) < 0)
		return (-1);
	*save_default = buffer;
	buffer.c_lflag &= ~(ECHO | ICANON);// | ISIG);
	buffer.c_cc[VMIN] = 1;
	buffer.c_cc[VTIME] = 0;
	if ((tcsetattr(fd, TCSAFLUSH, &buffer) < 0)
	|| (tcgetattr(fd, &buffer) < 0)
	|| ((buffer.c_lflag & (ECHO | ICANON/* | ISIG*/)) || buffer.c_cc[VMIN] != 1
			|| buffer.c_cc[VTIME] != 0))
	{
		tcsetattr(fd, TCSAFLUSH, save_default);
		write(2, "error while trying to change tty_setting\n", 41);
		return (-1);
	}
	return (0);
}

int		tty_reset(int fd, struct termios save_default)
{
	char *buf;

	if (tcsetattr(fd, TCSAFLUSH, &save_default) < 0)
		ft_write_error("don't back seting ICANON and ECHO!");

	if ((buf = tgetstr("ks", NULL)))
		ft_putstr(buf);
	else
		ft_write_error("termcap not keypad mode!");

	return (0);
}
